<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

/*Route::get('/', function()
{
	return View::make('hello');
});*/

Route::get('/', 'HomeController@home');

/*Route::get('signup', 'UserController@signup');
Route::post('signup', 'UserController@signup');*/

Route::any('signup', 'UserController@signup');
Route::any('login', 'UserController@login');
Route::get('aboutus', function()
{
	return View::make('about');
});

Route::post('checkUniqueEmail', 'AjaxController@checkUniqueEmail');

Route::get('verify/{token}', 'UserController@verifyEmail');

Route::any('profile', 'UserController@profile');

Route::any('editprofile', 'UserController@editProfile');

Route::get('logout', 'UserController@logout');

Route::any('forgotpassword', 'UserController@forgotPassword');

Route::any('resetpassword/{token}', 'UserController@resetPassword');

Route::any('getstateandcity', 'CommonController@getStateAndCity');

Route::get('myfamily', function()
{
	//return View::make('about');
});

/**
* Admin Routes
*/
Route::get('admin', function()
{
	return Redirect::to('admin/login');
});

Route::get('admin/login', 'admin\AdminLoginController@login');

Route::post('admin/login', 'admin\AdminLoginController@authLogin');

Route::get('admin/logout', 'admin\AdminLoginController@logout');

Route::get('admin/dashboard', 'admin\AdminDashboardController@dashboard');

Route::get('admin/notifications', 'admin\AdminNotificationsController@show');

Route::any('admin/addnotification', 'admin\AdminNotificationsController@add');

Route::any('admin/notification/edit/{id}', 'admin\AdminNotificationsController@edit');

Route::any('admin/notification/delete/{id}', 'admin\AdminNotificationsController@delete');

/**
* Session filter
* Authenticated user's allowed
*/
/*Route::filter('checkSession', function()
{
    if (!Session::has('adminUser')) {
			return Redirect::to('admin/login')->with('errorMessage', 'Session timed out. Please login again.');		
		} 
});*/

Route::get('admin/users', 'admin\AdminUserController@users');



