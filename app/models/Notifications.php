<?php

class Notifications extends Eloquent {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'notifications';
    
    public static function sendMail($to, $subject, $body) {
        $transport = Swift_SmtpTransport::newInstance();
        $mailer = Swift_Mailer::newInstance($transport);
        
        // Create a message
        $message = Swift_Message::newInstance($subject)
          ->setFrom(array('info@familytree.com' => 'Family Tree'))
          ->setTo(array($to))
          ->setBody($body)
          ->addPart($body, 'text/html')
          ;
        
        // Send the message
        $result = $mailer->send($message);
        
        return $result;
        
    }

}
