<?php

class User extends Eloquent {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'users';

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = array('password');
    
    protected function uploadImage($fileObject,$newFileName,$resizeWidth,$resizeHeight,$filePath) {
        $fileObject->move($filePath, $newFileName);
        Image::make(sprintf($filePath.'/%s', $newFileName))->resize($resizeWidth, $resizeHeight)->save();
    } 

}
