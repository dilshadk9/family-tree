<?php

class UserController extends BaseController {

    /**
     * The layout that should be used for responses.
    */
    protected $layout = 'layouts.master';
    
    /*public function __construct() {
		parent::__construct();
		$data = array(
							'ADMIN_LOGIN_TITLE' => Config::get('constants.ADMIN_LOGIN_TITLE'),
							'SITE_NAME' => Config::get('constants.SITE_NAME'),
							'ADMIN_PAGE_TITLE' => Config::get('constants.ADMIN_PAGE_TITLE'),
                );
						 
		View::share($data);
		if (!Session::has('adminUser')) {
			return Redirect::to('admin/login')->with('errorMessage', 'Session timed out. Please login again.');		
		} 		
	}*/

	public function signup()
	{
        if(Request::ajax())
        {
            $validator = Validator::make(
            array(
                'firstname' => Input::get('firstname'),
                'middlename' => Input::get('middlename'),
                'lastname' => Input::get('lastname'),
                'email' => Input::get('email'),
                'password' => Input::get('password'),
                'mobile' => Input::get('mobile'),
                'gender' => Input::get('gender')
    
            ),
            array(
                'firstname' => 'required',
                'middlename' => 'required',
                'lastname' => 'required',
                'email' => 'required|email|unique:users,email',
                'password' => 'required',
                'mobile' => 'required|numeric|min:10',
                'gender' => 'required'
            )
        );
        
        if ($validator->fails()) {
            $errors = $validator->messages();
            
            return Response::json(array('errors' => $errors));
        } else {
            
            $data = Input::all();
            $user = new User();
            $user->first_name = $data['firstname']; 
            $user->middle_name = $data['middlename'];
            $user->last_name = $data['lastname'];
            $user->email = $data['email'];
            $user->password =  Hash::make($data['password']);
            $user->mobile = $data['mobile'];
            $user->gender = $data['gender'];
            $user->token = $data['_token'];

            $user->save();
            
            $code = Crypt::encrypt(Input::get('email'));
            $to = Input::get('email');
            
            // Get notification template
            $verifsEmailTemplate = Notifications::find(1);
            $subject = $verifsEmailTemplate->title;
            $body = $verifsEmailTemplate->description;
            
            $message_body = str_replace(
            array('%%FULL_NAME%%','%%LINK%%'),
            array(Input::get('firstname').' '.Input::get('lastname'), "http://www.standardethics.com/familytree/verify/$code"),
            $body
            );

            Notifications::sendMail($to, $subject, $message_body);

            return Response::json(array('success' => true));
            //return Redirect::to('signup')->with('successSignup', 'Your account has been created successfully! <br> Verification link has been sent to your register email address.');
        }
 
        }
        else
        {
            $this->layout->content = View::make('signup');
        }
		
	}
    
    public function verifyEmail($token) {
        $email = Crypt::decrypt($token);
        $user = DB::table('users')->where('email', $email)->first();
        if(count($user) > 0) {
            DB::table('users')
            ->where('email', $email)
            ->update(array('status' => 1));
            return Redirect::to('login')->with('successLogin', 'Your account has been successfully verified.');
        } else {
            return Redirect::to('login');
        }
    }
    
    public function login() {
        if (Request::isMethod('post')) {
            $user = new User();
            $loginCheck = User::where('email', '=', Input::get('email'))->get();
            if(!empty($loginCheck[0])) { 
                if(Hash::check(Input::get('password'), $loginCheck[0]->password)) {                   
                    if($loginCheck[0]->status == 0) {
                        return Redirect::to('login')->with('errorLogin', 'Please verify your email address.'); 
                    }
                    else {
                        //return Redirect::to('login')->with('successLogin', 'Login Successfull.');
                        Session::put('id', $loginCheck[0]->id);
                        return Redirect::to('profile');    
                    }       
                                   
                } 
                else {
                    return Redirect::to('login')->with('errorLogin', 'Your email or password is incorrect.');
                }
            } 
            else {
                return Redirect::to('login')->with('errorLogin', 'Your email or password is incorrect.');
            }
        } 
        else {
            $this->layout->content = View::make('login');    
        } 
        
    }
    
    public function editProfile() {
      if (!Session::has('id')) {
			return Redirect::to('login')->with('errorLogin', 'Session timed out. Please login again.');		
      }   
      if (Request::isMethod('post')) {
        // Validation
        $validator = Validator::make(
            array(
                'firstname' => Input::get('firstname'),
                'middlename' => Input::get('middlename'),
                'lastname' => Input::get('lastname'),
                'date of birth' => Input::get('dob'),
                'mobile' => Input::get('mobile'),
                'gender' => Input::get('gender'),
                'spouse' => Input::get('spouse'),
                'pob' => Input::get('pob'),
                'country' => Input::get('country'),
                'state' => Input::get('state'),
                'city' => Input::get('city'),
                'gotra' => Input::get('gotra'),
                'father first name' => Input::get('father_fname'),
                'mother first name' => Input::get('mother_fname'),
                'age' => Input::get('age_range'),
                'profile pic' => Input::file('profile_pic')
    
            ),
            array(
                'firstname' => 'required',
                'middlename' => 'required',
                'lastname' => 'required',
                'date of birth' => 'required',
                'mobile' => 'required|numeric|min:10',
                'gender' => 'required',
                'spouse' => 'required',
                'pob' => 'required',
                'country' => 'required',
                'state' => 'required',
                'city' => 'required',
                'gotra' => 'required',
                'father first name' => 'required',
                'mother first name' => 'required',
                'age' => 'required',
                'profile pic' => 'required|image|mimes:jpeg,jpg,png|max:1000'
            )
         );
         
        if ($validator->fails()) {
             if(Session::has('myfamily'))
             Session::forget('myfamily');
             $errors = $validator->messages();
             return Redirect::back()
             ->withErrors($errors)
             ->withInput();
        } 
        $data = Input::all();
        $user = User::find(Session::get('id'));
        if(!is_dir('uploadsdata/'.Session::get('id'))) {
            mkdir('uploadsdata/'.Session::get('id'));
        }
        
        $file = Input::file('profile_pic');        
        $filePath = 'uploadsdata/'.Session::get('id');
        $original_name = $file->getClientOriginalName();
        $file_name = pathinfo($original_name, PATHINFO_FILENAME); // file
        $extension = pathinfo($original_name, PATHINFO_EXTENSION); // jpg       
        $newFileName = $file_name.'_'.time().'.'.$extension;

        User::uploadImage($file,$newFileName,$resizeWidth = 101,$resizeHeight = 108,$filePath);        
        
        
        $user->first_name = $data['firstname']; 
        $user->middle_name = $data['middlename'];
        $user->last_name = $data['lastname'];
        $user->dob = date('Y-m-d',strtotime($data['dob']));
        $user->mobile = $data['mobile'];
        $user->gender = $data['gender'];
        $user->spouse = $data['spouse'];
        $user->pob = $data['pob'];
        $user->city = $data['city'];
        $user->state = $data['state'];
        $user->country = $data['country'];
        $user->father_fname = $data['father_fname'];
        $user->mother_fname = $data['mother_fname'];
        $user->age_range = $data['age_range'];
        $user->gotra = $data['gotra'];
        $user->profile_pic = $newFileName;
        $user->save();
        Session::put('myfamily',true);
        return Redirect::to('profile')->with('successProfile', 'Your profile has been updated successfully.');
      }
      else
      {
        $id = Session::get('id');
        $user = DB::select('select *,l1.name as state, l2.name as city,l1.location_id as state_id, l2.location_id as city_id
                            from users u
                            left join location l1 on u.state = l1.location_id
                            left join location l2 on u.city = l2.location_id
                            where u.id = ?', array($id));
		$countries = DB::select('select * from location where location_type = ?', array(0));
        $data = array('user' => $user, 'countries' => $countries);
        $this->layout->content = View::make('editprofile', $data);     
      }  
      
    }
    
    public function profile() {
        if (!Session::has('id')) {
			return Redirect::to('login')->with('errorLogin', 'Session timed out. Please login again.');		
        } 
        $id = Session::get('id');
        $user = DB::select('select *,l1.name as state, l2.name as city,l1.location_id as state_id, l2.location_id as city_id,l3.name as countryName
                            from users u
                            left join location l1 on u.state = l1.location_id
                            left join location l2 on u.city = l2.location_id
                            left join location l3 on u.country = l3.location_id
                            where u.id = ?', array($id)); 
        $data = array('user' => $user);
        $this->layout->content = View::make('profile', $data);  
    }
    
    public function logout() {
        Session::forget('id');
        Session::flush();
        Session::regenerate();
        return Redirect::to('login')->with('successLogin', 'Your have been logged out successfully.'); 
    }
    
    public function forgotPassword() {
        if (Request::isMethod('post')) {
            $user = User::where('email', '=', Input::get('email'))->take(10)->get();
            if(!empty($user[0]))
            {
                $to = $user[0]->email;
                $token = Crypt::encrypt($user[0]->email);;
                
                $resetPassTemplate = Notifications::find(2);
                $subject = $resetPassTemplate->title;
                $body = $resetPassTemplate->description;
                
                $message_body = str_replace(
                array('%%FULL_NAME%%','%%LINK%%'),
                array($user[0]->first_name.' '.$user[0]->last_name, "http://www.standardethics.com/familytree/resetpassword/$token"),
                $body
                );
                Notifications::sendMail($to, $subject, $message_body);
                return Redirect::to('login')->with('successLogin', 'Password reset link has been sent to your email address.'); 
            }
            else
            {
                return Redirect::to('forgotpassword')->with('errorForgotPass', 'Unable to find your email address in our database.');    
            }
            
        }
        else
        {
          $this->layout->content = View::make('forgotpassword');   
        }
    }
    
    public function resetPassword($token) {
        if (Request::isMethod('post')) {
           $email = Crypt::decrypt($token);
           $user = User::where('email', '=', $email)->take(10)->get();
           if(!empty($user[0]))
           {
            DB::table('users')
            ->where('email', $email)
            ->update(array('password' => Hash::make(Input::get('password'))));
            return Redirect::to('login')->with('successLogin', 'Password has been changed successfully.');
           }
           else
           {
             return Redirect::to('login')->with('errorLogin', 'Request Denied. Please try again.');
           }
        }
        else
        {
           $this->layout->content = View::make('resetpassword');   
        }
         
    }
     

}
