<?php

class CommonController extends BaseController {
    
	public function getStateAndCity()
	{
        if(Request::ajax())
        {
			$location_id = Input::get('location_id'); //Country ID
			$location_type = Input::get('location_type');
			$types = array('Country', 'State', 'City');
			$result = DB::select('select * from location where location_type = ? and parent_id = ?', array($location_type, $location_id));
			echo '<option value="">Select '.$types[$location_type].'</option>';
			foreach ($result as $value) {
				echo "<option value='" . $value->location_id . "'>" . $value->name . "</option>";
			}
            
        }
     }   

}
