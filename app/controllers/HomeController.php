<?php

class HomeController extends BaseController {

    /**
     * The layout that should be used for responses.
    */
    protected $layout = 'layouts.master';
    
    /*public function __construct() {
		parent::__construct();
		$data = array(
							'ADMIN_LOGIN_TITLE' => Config::get('constants.ADMIN_LOGIN_TITLE'),
							'SITE_NAME' => Config::get('constants.SITE_NAME'),
							'ADMIN_PAGE_TITLE' => Config::get('constants.ADMIN_PAGE_TITLE'),
                );
						 
		View::share($data);
		if (!Session::has('adminUser')) {
			return Redirect::to('admin/login')->with('errorMessage', 'Session timed out. Please login again.');		
		} 		
	}*/

	public function home()
	{
		$this->layout->content = View::make('home');
	}

}
