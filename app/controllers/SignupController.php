<?php

class SignupController extends BaseController {

    /**
     * The layout that should be used for responses.
    */
    protected $layout = 'layouts.master';
    
    /*public function __construct() {
		parent::__construct();
		$data = array(
							'ADMIN_LOGIN_TITLE' => Config::get('constants.ADMIN_LOGIN_TITLE'),
							'SITE_NAME' => Config::get('constants.SITE_NAME'),
							'ADMIN_PAGE_TITLE' => Config::get('constants.ADMIN_PAGE_TITLE'),
                );
						 
		View::share($data);
		if (!Session::has('adminUser')) {
			return Redirect::to('admin/login')->with('errorMessage', 'Session timed out. Please login again.');		
		} 		
	}*/

	public function signup()
	{
        if (Request::isMethod('post'))
        {
            $data = Input::all();
            $signup = new Signup();
            $signup->first_name = $data['fname']; 
            $signup->middle_name = $data['mname'];
            $signup->last_name = $data['lname'];
            $signup->email = $data['email'];
            $signup->password =  Hash::make($data['password']);
            $signup->mobile = $data['mobile'];
            $signup->gender = $data['gender'];
            $signup->token = $data['_token'];
            $signup->save();
            return Redirect::to('signup')->with('successSignup', 'Your account has been created successfully! <br> Verification link has been sent to your registered email address.');
        }
        else
        {
            $this->layout->content = View::make('signup');
        }
		
	}
    
    public function checkEmailExists()
    {
        
        ob_clean();
        if(Request::ajax())
        {
            print_r($_POST);
        }
        else
        {
            die('Direct script access not allowed');
        }
    }
    

}
