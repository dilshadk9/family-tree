<?php

class AjaxController extends BaseController {
    
    public function checkUniqueEmail()
    {
        if(Request::ajax())
        {
            $data = Input::all();
            $email = $data['email'];
            $user = User::where('email','=',$email)->count();
            
            if($user) {
                echo '1';
            } else {
                echo '0';
            }
            
        }
        else
        {
            die('Direct script access not allowed');
        }
    }

    
}
