<?php

class BaseController extends Controller {

	/**
	 * Setup the constructor
	 *
	 * @return void
	 */

	public function __construct() {
		// Run the 'csrf' filter on all post, put, patch and delete requests.
		$this->beforeFilter('csrf', ['on' => ['post', 'put', 'patch', 'delete']]);
		$this->beforeFilter('checkSession');
		/*$data = array(
						'ADMIN_LOGIN_TITLE' => Config::get('constants.ADMIN_LOGIN_TITLE'),
						'SITE_NAME' => Config::get('constants.SITE_NAME'),
						'ADMIN_PAGE_TITLE' => Config::get('constants.ADMIN_PAGE_TITLE'),
				);
						 
		View::share($data);*/
	}

	/**
	 * Setup the layout used by the controller.
	 *
	 * @return void
	 */
	protected function setupLayout()
	{
		if ( ! is_null($this->layout))
		{
			$this->layout = View::make($this->layout);
		}
	}

}
