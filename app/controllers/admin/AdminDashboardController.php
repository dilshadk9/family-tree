<?php 
namespace admin;
use View, Session, Redirect, Config;

class AdminDashboardController extends \BaseController {

	/**
     * The layout that should be used for responses.
     */
    protected $layout = 'layouts.admin.master';
	
	public function __construct() {
		parent::__construct();
		$data = array(
									'ADMIN_LOGIN_TITLE' => Config::get('constants.ADMIN_LOGIN_TITLE'),
									'SITE_NAME' => Config::get('constants.SITE_NAME'),
									'ADMIN_PAGE_TITLE' => Config::get('constants.ADMIN_PAGE_TITLE'),
						 );
						 
		View::share($data);
		if (!Session::has('adminUser')) {
			return Redirect::to('admin/login')->with('errorMessage', 'Session timed out. Please login again.');		
		} 		
	}

	public function dashboard() {
		$this->layout->content = View::make('admin.dashboard');
	}
	
	public function users() {
		$this->layout->content = View::make('admin.users');
	}
	
	

}
