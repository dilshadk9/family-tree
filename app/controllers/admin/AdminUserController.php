<?php 
namespace admin;
use View, Session, Redirect, Config;

class AdminUserController extends \BaseController {

	/**
     * The layout that should be used for responses.
     */
    protected $layout = 'layouts.admin.master';
	
	public function __construct() {
		parent::__construct();
		$data = array(
									'ADMIN_LOGIN_TITLE' => Config::get('constants.ADMIN_LOGIN_TITLE'),
									'SITE_NAME' => Config::get('constants.SITE_NAME'),
									'ADMIN_PAGE_TITLE' => Config::get('constants.ADMIN_PAGE_TITLE'),
						 );
						 
		View::share($data);
		if (!Session::has('adminUser')) {
			return Redirect::to('admin/login')->with('errorMessage', 'Session timed out. Please login again.');		
		} 		
	}

	
	public function users() {
	    if (!Session::has('adminUser')) {
			return Redirect::to('admin/login')->with('errorMessage', 'Session timed out. Please login again.');		
		} 
		$users = AdminUser::select('first_name','middle_name','last_name','email','mobile','gender','status','created_at')->get();
        Session::put('usersCount', count($users));
		$data = array('users' => $users);
		$this->layout->content = View::make('admin.users', $data);
	}
	
	

}
