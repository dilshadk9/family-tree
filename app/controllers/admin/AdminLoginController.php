<?php 
namespace admin;
use View, Config, Input, Hash, Redirect, Session; 

class AdminLoginController extends \BaseController {

	public function __construct() {
		$data = array(
									'ADMIN_LOGIN_TITLE' => Config::get('constants.ADMIN_LOGIN_TITLE'),
									'SITE_NAME' => Config::get('constants.SITE_NAME'),
									'ADMIN_PAGE_TITLE' => Config::get('constants.ADMIN_PAGE_TITLE'),
						 );
						 
		View::share($data);		 
	}

	public function login() {
		return View::make('admin.login');
	}
	
	public function authLogin() {
		$data = Input::all();
		$adminLogin = AdminLogin::find(1);
		$username = $adminLogin->username;
		$hashedPassword = $adminLogin->password;
		$_token = csrf_token();
		if($data['username']  == $username  && Hash::check($data['password'], $hashedPassword) && $data['_token'] == $_token) {
			Session::put('adminUser', $data['username']);
			return Redirect::to('admin/users');
		} else {
			return Redirect::to('admin/login')->with('errorMessage', 'There was a problem with your login.');
		}	
	}
	
	public function logout() {
		Session::forget('adminUser');
		Session::flush();
		return Redirect::to('admin/login')->with('successMessage', 'You are logged out.');
	}
	
}
