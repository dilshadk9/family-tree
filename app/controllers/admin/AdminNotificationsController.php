<?php 
namespace admin;
use View, Session, Redirect, Config, Request, Input;

class AdminNotificationsController extends \BaseController {

	/**
     * The layout that should be used for responses.
     */
    protected $layout = 'layouts.admin.master';
	
	public function __construct() {
		parent::__construct();
		$data = array(
									'ADMIN_LOGIN_TITLE' => Config::get('constants.ADMIN_LOGIN_TITLE'),
									'SITE_NAME' => Config::get('constants.SITE_NAME'),
									'ADMIN_PAGE_TITLE' => Config::get('constants.ADMIN_PAGE_TITLE'),
						 );
						 
		View::share($data);
		if (!Session::has('adminUser')) {
			return Redirect::to('admin/login')->with('errorMessage', 'Session timed out. Please login again.');		
		} 		
	}

	public function show() {
	    
        $notifs = AdminNotifications::all();
        $data = array('notifs' => $notifs);
	    $this->layout->content = View::make('admin.notification', $data); 
	}
    
    public function add() {		
        if (Request::isMethod('post')) {
            $data = Input::all();
            $notif = new AdminNotifications();
            $notif->title = $data['title']; 
            $notif->description = $data['description'];
            $notif->save();
            return Redirect::to('admin/addnotification')->with('notifsMessage', 'Notification added successfully.');	
        }
        else {
           $this->layout->content = View::make('admin.addnotification'); 
        }
        
	}
    
    public function edit($id) {
        if (Request::isMethod('post')) {
            $notifs = AdminNotifications::find($id);
            $notifs->title = Input::get('title');
            $notifs->description = Input::get('description');
            $notifs->save(); 
            return Redirect::to('admin/notification/edit/'.$id)->with('notifsMessage', 'Notification Updated successfully.');
        }
        else {
            $notifs = AdminNotifications::find($id);
            $data = array('notifs' => $notifs);
            $this->layout->content = View::make('admin.editnotification', $data);
        }
    }
    
    public function delete($id) {
        $notif = AdminNotifications::find($id);
        $notif->delete();
        return Redirect::to('admin/notifications')->with('notifsMessage', 'Notification deleted successfully.');
    }
	
	

}
