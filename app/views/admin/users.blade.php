@extends('layouts.admin.master')
@section('content')
<div class="span9">
			<div class="content">
				
				<div class="module">
					<div class="module-head">
						<h3>
							Users</h3>
					</div>
					<div class="module-body table">
						<table cellpadding="0" cellspacing="0" border="0" class="datatable-1 table table-bordered table-striped	 display"
							width="100%">
							<thead>
								<tr>
									<th>
										Full Name
									</th>
									<th>
										Email Id
									</th>
									<th>
										Gender
									</th>
									<th>
										Mobile
									</th>
									<th>
										Registered Date
									</th>
                                    <th>
										Status
									</th>
								</tr>
							</thead>
							<tbody>
								@foreach($users as $user)
								<tr class="odd gradeX">
									<td>{{ $user->first_name.' '.$user->middle_name.' '.$user->last_name }}</td>
									<td>{{ $user->email}}</td>
									<td>{{ $user->gender}}</td>
									<td>{{ $user->mobile}}</td>
									<td>{{ $user->created_at}}</td>
                                    <td>@if (($user->status) == 1) 
                                    Active 
                                    @else
                                    Disable 
                                    @endif</td>
								</tr>
								@endforeach
							</tbody>
							<tfoot>
								<tr>
									<th>
										Full Name
									</th>
									<th>
										Email Id
									</th>
									<th>
										Gender
									</th>
									<th>
										Mobile
									</th>
									<th>
										Registered Date
									</th>
                                    <th>
										Status
									</th>
								</tr>
							</tfoot>
						</table>
					</div>
				</div>
				<!--/.module-->
			</div>
			<!--/.content-->
		</div>
		<!--/.span9-->	
@stop