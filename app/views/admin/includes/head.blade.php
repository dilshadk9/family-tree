<head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>{{ $SITE_NAME }}</title>
		{{ HTML::style('assets/admin/bootstrap/css/bootstrap.min.css'); }}
		{{ HTML::style('assets/admin/bootstrap/css/bootstrap-responsive.min.css'); }}
		{{ HTML::style('assets/admin/css/theme.css'); }}
        {{ HTML::style('assets/admin/images/icons/css/font-awesome.css'); }}
        <link type="text/css" href='http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600'
            rel='stylesheet'>
</head>