<div class="footer">
            <div class="container">
                <b class="copyright">&copy; {{ date('Y') }} {{ $SITE_NAME }} </b>All rights reserved.
            </div>
</div>
{{ HTML::script('assets/admin/scripts/jquery-1.9.1.min.js'); }}
{{ HTML::script('assets/scripts/jquery.js'); }}
{{ HTML::script('assets/admin/scripts/jquery-ui-1.10.1.custom.min.js'); }}
{{ HTML::script('assets/admin/bootstrap/js/bootstrap.min.js'); }}
{{ HTML::script('assets/admin/scripts/flot/jquery.flot.js'); }}
{{ HTML::script('assets/admin/scripts/flot/jquery.flot.resize.js'); }}
{{ HTML::script('assets/admin/scripts/datatables/jquery.dataTables.js'); }}
{{ HTML::script('assets/admin/scripts/common.js'); }}
{{ HTML::script('assets/admin/scripts/ckeditor/ckeditor.js'); }}
