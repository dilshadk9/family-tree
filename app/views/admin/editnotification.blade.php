@extends('layouts.admin.master')
@section('content')
<div class="span9">
					<div class="content">

						<div class="module">
							<div class="module-head">
								<h3>Update Notification</h3>
							</div>
							<div class="module-body">
									{{ Form::open(array('name' => 'notification', 'method' => 'post', 'id' => 'notification', 'class' => 'form-horizontal row-fluid' )) }}
                                    <input type="hidden" name="id" value="{{$notifs->id}}"/>
                                    @if (Session::get('notifsMessage'))	
    									<div class="alert alert-success">
    										<a href="#" class="close" data-dismiss="alert">&times;</a>
    										<strong>Success!</strong> {{ Session::get('notifsMessage') }}
    									</div>
        							@endif
										<div class="control-group">
											<label class="control-label" for="basicinput">Title</label>
											<div class="controls">
												<input type="text" name="title" id="title" value="{{$notifs->title}}" placeholder="Title" class="span8" required>
												<!--<span class="help-inline">Minimum 5 Characters</span>-->
											</div>
										</div>

										<div class="control-group">
											<label class="control-label" for="basicinput">Description</label>
											<div class="controls">
												<textarea class="span8" rows="5" name="description" id="description" required>{{$notifs->description}}</textarea>
											</div>
										</div>

										<div class="control-group">
											<div class="controls">
												<button type="submit" class="btn">Submit</button>
                                                <button type="button" class="btn" onclick="window.location = '{{ URL::to('admin/notifications') }}';">Back</button>
											</div>
										</div>
									{{ Form::close() }}	
							</div>
						</div>
<script>
   window.onload = function()
   {
   	CKEDITOR.replace('description');
   };
</script>
						
						
					</div><!--/.content-->
				</div>
		<!--/.span9-->	
@stop
