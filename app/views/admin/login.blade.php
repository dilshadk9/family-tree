<!DOCTYPE html>
<html lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>{{ $SITE_NAME }}</title>
	{{ HTML::style('assets/admin/bootstrap/css/bootstrap.min.css'); }}
	{{ HTML::style('assets/admin/bootstrap/css/bootstrap-responsive.min.css'); }}
	{{ HTML::style('assets/admin/css/theme.css'); }}
	{{ HTML::style('assets/admin/images/icons/css/font-awesome.css'); }}
	<link type="text/css" href='http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600' rel='stylesheet'>
</head>
<body>

	<div class="navbar navbar-fixed-top">
		<div class="navbar-inner">
			<div class="container">
				<a class="btn btn-navbar" data-toggle="collapse" data-target=".navbar-inverse-collapse">
					<i class="icon-reorder shaded"></i>
				</a>

			  	<a class="brand" href="javascript:void(0);">
			  		{{ $ADMIN_PAGE_TITLE }}
			  	</a>

				<div class="nav-collapse collapse navbar-inverse-collapse">
				
					<!--<ul class="nav pull-right">

						<li><a href="#">
							Sign Up
						</a></li>

						

						<li><a href="#">
							Forgot your password?
						</a></li>
					</ul>-->
				</div><!-- /.nav-collapse -->
			</div>
		</div><!-- /navbar-inner -->
	</div><!-- /navbar -->



	<div class="wrapper">
		<div class="container">
			<div class="row">
				<div class="module module-login span4 offset4">
					<!--<form class="form-vertical">-->
					{{ Form::open(array('url' => '/admin/login', 'class' => 'form-vertical', 'method' => 'post' )) }}
						<div class="module-head">
							<h3>Sign In</h3>
						</div>
						<div class="module-body">
							<div class="control-group">
								<div class="controls row-fluid">
									<input class="span12" type="text" id="inputEmail" name="username" placeholder="Username" required>
								</div>
							</div>
							<div class="control-group">
								<div class="controls row-fluid">
									<input class="span12" type="password" id="inputPassword" name="password" placeholder="Password" required>
								</div>
							</div>
						</div>
						<div class="module-foot">
							<div class="control-group">
								<div class="controls clearfix">
									<button type="submit" class="btn btn-primary pull-right">Login</button>
									<!--<label class="checkbox">
										<input type="checkbox"> Remember me
									</label>--> <br><br>
								</div>
							</div>
							@if (Session::get('errorMessage'))	
									<div class="alert alert-error">
										<a href="#" class="close" data-dismiss="alert">&times;</a>
										<strong>Error!</strong> {{ Session::get('errorMessage') }}
									</div>
							@endif
							@if (Session::get('successMessage'))	
									<div class="alert alert-success">
										<a href="#" class="close" data-dismiss="alert">&times;</a>
										<strong>Success!</strong> {{ Session::get('successMessage') }}
									</div>
							@endif
						</div>
					{{ Form::close() }}
				</div>
			</div>
		</div>
	</div><!--/.wrapper-->

	<div class="footer">
		<div class="container">
			 

			<b class="copyright">&copy; {{ date('Y')}} {{ $SITE_NAME }} </b> All rights reserved.
		</div>
	</div>
	{{ HTML::script('assets/admin/scripts/jquery-1.9.1.min.js'); }}
	{{ HTML::script('assets/admin/scripts/jquery-ui-1.10.1.custom.min.js'); }}
	{{ HTML::script('assets/admin/bootstrap/js/bootstrap.min.js'); }}
</body>