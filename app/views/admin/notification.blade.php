@extends('layouts.admin.master')
@section('content')
<div class="span9">
			<div class="content">
				
				<div class="module">
					<div class="module-head">
						<h3>Notifications <p style="float: right;"><a href="./addnotification">Add New</a></p></h3>
					</div>
					<div class="module-body table">
                    @if (Session::get('notifsMessage'))	
    									<div class="alert alert-success">
    										<a href="#" class="close" data-dismiss="alert">&times;</a>
    										<strong>Success!</strong> {{ Session::get('notifsMessage') }}
    									</div>
        			@endif
						<table cellpadding="0" cellspacing="0" border="0" class="datatable-1 table table-bordered table-striped	 display"
							width="100%">
							<thead>
								<tr>
									<th width="25%">
										Title
									</th>
									<th width="50%">
										Description
									</th>
                                    <th>
										Created Date
									</th>
									<th width="20%">
										Action
									</th>
								</tr>
							</thead>
							<tbody>
								@foreach($notifs as $notif)
								<tr class="odd gradeX">
                                <td>{{ $notif->title}}</td>
                                <td>{{ $notif->description}}</td>
                                <td>{{ $notif->created_at}}</td>
                                <td><a href="./notification/edit/{{$notif->id}}">Edit</a> | <a href="./notification/delete/{{$notif->id}}" onclick="return confirm('Do you want to delete?');">Delete</a></td>
                                </tr>
                                @endforeach
							</tbody>
							<tfoot>
								<tr>
									<th>
										Title
									</th>
									<th>
										Description
									</th>
									<th>
										Created Date
									</th>
									<th>
										Action
									</th>
								</tr>
							</tfoot>
						</table>
					</div>
				</div>
				<!--/.module-->
			</div>
			<!--/.content-->
		</div>
		<!--/.span9-->	
@stop