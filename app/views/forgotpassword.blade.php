@extends('layouts.master')
@section('content')
<div id="contentOuterSeparator"></div>

<div class="container">

    <div class="divPanel page-content">

        <div class="row-fluid">

                <div class="span12" id="divMain">

                    <h1>Recover your password</h1> <hr />

                       <div class="row-fluid">
                <div class="span8" id="divMain">
                            @if (Session::get('successForgotPass'))	
									<div class="alert alert-success">
										<a href="#" class="close" data-dismiss="alert">&times;</a>
										<strong>Success!</strong> {{ Session::get('successForgotPass') }}
									</div>
							@endif
                            @if (Session::get('errorForgotPass'))	
									<div class="alert alert-error">
										<a href="#" class="close" data-dismiss="alert">&times;</a>
										<strong>Error!</strong> {{ Session::get('errorForgotPass') }}
									</div>
							@endif
			<!--Start Contact form -->		                                                
{{ Form::open(array('name' => 'login', 'method' => 'post', 'id' => 'login' )) }}
  <fieldset>
    
	<input type="email" name="email" id="email" value=""  class="input-block-level" placeholder="Email" required/>
	    <div class="actions">
	<input type="submit" value="Recover" name="submit" id="submitButton" class="btn btn-info pull-left" title="Click here to send your password!" />
    	</div>
	
	</fieldset>
{{ Form::close() }}	
		 
			<!--End Contact form -->											 
                </div>
				
            </div>			


                </div>

            </div>

        <div id="footerInnerSeparator"></div>
    </div>

</div>
@stop

