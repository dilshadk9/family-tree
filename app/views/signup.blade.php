@extends('layouts.master')
@section('content')
<div id="contentOuterSeparator"></div>

<div class="container">

    <div class="divPanel page-content">

        <div class="row-fluid">

                <div class="span12" id="divMain">

                    <h1>Create an account</h1> <hr />

                       <div class="row-fluid">
                <div class="span8" id="divMain">
                            @if (Session::get('successSignup'))	
									<div class="alert alert-success">
										<a href="#" class="close" data-dismiss="alert">&times;</a>
										<strong>Success!</strong> {{ Session::get('successSignup') }}
									</div>
							@endif
			<!--Start Contact form -->		                                                
{{ Form::open(array('name' => 'signup', 'method' => 'post', 'id' => 'signup' )) }}
  <fieldset>
    
	<input type="text" name="firstname" id="firstname" value=""  class="input-block-level" placeholder="First Name" />
	<input type="text" name="middlename" id="middlename" value=""  class="input-block-level" placeholder="Middle Name" />
	<input type="text" name="lastname" id="lastname" value=""  class="input-block-level" placeholder="Last Name" />
    <input type="text" name="email" id="email" value="" class="input-block-level" placeholder="Email" />
    <input type="password" name="password" id="password" value="" class="input-block-level" placeholder="Password" />
    <input type="text" name="mobile" id="mobile" value="" class="input-block-level" placeholder="Mobile" />
      <select class="form-control input-block-level" id="gender" name="gender">
        <option value="">Select</option>
        <option value="Male">Male</option>
        <option value="Female">Female</option>
        <option value="Other">Other</option>
      </select>
    <div class="actions">
	<input type="submit" value="Sign up" name="submit" id="submitButton" class="btn btn-info pull-left" title="Click here to register!" />
	</div>
	
	</fieldset>
{{ Form::close() }}				 
			<!--End Contact form -->											 
                </div>
				
            </div>			


                </div>

            </div>

        <div id="footerInnerSeparator"></div>
    </div>

</div>
<div id="myModal" class="modal fade" style="display: none;">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Confirmation</h4>
                </div>
                <div class="modal-body">
                    <p>Verification link has been sent to your register email address.</p>
                    <p class="text-warning"><small>Please verify your email address to continue.</small></p>
                </div>
                <div class="modal-footer">
                    <!--<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>-->
                    <button type="button" class="btn btn-primary continue">Continue</button>
                </div>
            </div>
        </div>
</div>
@stop

