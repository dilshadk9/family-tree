<!DOCTYPE html>
<html lang="en">
	@section('head')
		@include('admin.includes.head')
    @show
    <body>
        @section('header')
			@include('admin.includes.header')
		@show
        <!-- /navbar -->
        <div class="wrapper">
            <div class="container">
	<div class="row">
		
		@section('navigation')
			@include('admin.includes.navigation')
		@show	

		@yield('content')
	</div>
</div>
<!--/.container-->
        </div>
        <!--/.wrapper-->
        @section('footer')
			@include('admin.includes.footer')
		@show
</body>
</html>
