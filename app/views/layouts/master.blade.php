<!DOCTYPE HTML>
<html>
@section('head')
		@include('includes.head')
@show
<body id="pageBody">
<noscript>
<div id="enableJavascript">
        <div class="alert alert-danger" role="alert" style="text-align: center;font-size: 20px;">Warning! Please enable your javascript</div>
</div>
</noscript>
@section('navigation')
		@include('includes.navigation')
@show

@yield('home-banner')

@yield('content')

@section('footer')
        @include('includes.footer')
@show

</body>
</html>
