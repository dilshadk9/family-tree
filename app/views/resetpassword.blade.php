@extends('layouts.master')
@section('content')
<div id="contentOuterSeparator"></div>

<div class="container">

    <div class="divPanel page-content">

        <div class="row-fluid">

                <div class="span12" id="divMain">

                    <h1>Reset password</h1> <hr />

                       <div class="row-fluid">
                <div class="span8" id="divMain">
                            @if (Session::get('successLogin'))	
									<div class="alert alert-success">
										<a href="#" class="close" data-dismiss="alert">&times;</a>
										<strong>Success!</strong> {{ Session::get('successLogin') }}
									</div>
							@endif
                            @if (Session::get('errorLogin'))	
									<div class="alert alert-error">
										<a href="#" class="close" data-dismiss="alert">&times;</a>
										<strong>Error!</strong> {{ Session::get('errorLogin') }}
									</div>
							@endif
			<!--Start Contact form -->		                                                
{{ Form::open(array('name' => 'login', 'method' => 'post', 'id' => 'login' )) }}
  <fieldset>
    
	<input type="password" name="password" id="password" value=""  class="input-block-level" placeholder=" New Password" required/>
	    <div class="actions">
	<input type="submit" value="Reset password" name="submit" id="submitButton" class="btn btn-info pull-left" title="Click here to reset your password!" />
    	</div>
	
	</fieldset>
{{ Form::close() }}	
		 
			<!--End Contact form -->											 
                </div>
				
            </div>			


                </div>

            </div>

        <div id="footerInnerSeparator"></div>
    </div>

</div>
@stop
