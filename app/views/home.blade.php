@extends('layouts.master')
@section('home-banner')
<div id="decorative1" style="position:relative">
    <div class="container">

        <div class="divPanel headerArea">
            <div class="row-fluid">
                <div class="span12">

                        <div id="headerSeparator"></div>

                        <div id="divHeaderText" class="page-content">
                            <div id="divHeaderLine1">Create your own family tree!</div><br />
                            <div id="divHeaderLine2">The family is one of nature's masterpieces.</div><br />
                            <div id="divHeaderLine3"><a class="btn btn-large btn-primary" href="./signup">Start with your free account</a></div>
                        </div>

                        <div id="headerSeparator2"></div>

                </div>
            </div>

        </div>

    </div>
</div>
@stop
@section('content')
<!--<div id="contentOuterSeparator"></div>

<div class="container">

    <div class="divPanel page-content">

        <div class="row-fluid">

                <div class="span12" id="divMain">

                    <<h1>Welcome to family tree</h1>

                    <p>Create a free account, and start exploring your family or create a new family. <a href="./signup" title="Create an account">Signup</a></p>

                    hr style="margin:45px 0 35px" />

                    <div class="row-fluid">
                        <div class="span8">

                            <h3>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</h3>         

                            <p>
                                <img src="{{URL::to('assets/images/family-group.jpg')}}" class="img-polaroid" style="margin:12px 0px;">  
                            </p>
							
                            <p>Content on this page is for presentation purposes only. Lorem Ipsum is simply dummy text of the printing and typesetting industry. 
                            Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. 
							Lorem Ipsum is simply dummy text of the printing and typesetting industry. 
                            </p>
                                                                                       
                        </div>
                        <div class="span4 sidebar">

                            <div class="sidebox">
                                <h3 class="sidebox-title">Sample Sidebar Content</h3>
                                <p>Lorem Ipsum is simply dummy text of the printing and <a href="#">typesetting industry</a>. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.</p>                       
                            </div>

                            <br />

                            <div class="sidebox">
                                <h3 class="sidebox-title">Sample Sidebar Content</h3>
                                <p>
                                    <div class="input-append">
                                        <input class="span8" id="inpEmail" size="16" type="text"><button class="btn" type="button">Action</button>
                                    </div>
                                </p>                      
                            </div>

                        </div>
                    </div>

                </div>

            </div>

        <div id="footerInnerSeparator"></div>
    </div>

</div>-->
@stop
@section('footer')
<div class="container" style="padding-top: 10px;text-align: center;">
    <div class="row-fluid">
        <div class="span12">
            <p class="copyright">
                Copyright &copy; {{date('Y')}} Family Tree. All Rights Reserved.
            </p>

            <p class="social_bookmarks">
                <a href="#"><i class="social foundicon-facebook"></i> Facebook</a>
    			<a href=""><i class="social foundicon-twitter"></i> Twitter</a>
    			<a href="#"><i class="social foundicon-pinterest"></i> Pinterest</a>
    			<a href="#"><i class="social foundicon-rss"></i> Rss</a>
            </p>
        </div>
   </div>
</div>
@overwrite