@extends('layouts.master')
@section('content')
<div id="contentOuterSeparator"></div>

<div class="container">

    <div class="divPanel page-content">

        <div class="row-fluid">

                <div class="span12" id="divMain">

                    <h1>Your Profile</h1> <hr />

                       <div class="row-fluid">
                <div class="span8" id="divMain">
                            @if (Session::get('successProfile'))	
									<div class="alert alert-success">
										<a href="#" class="close" data-dismiss="alert">&times;</a>
										<strong>Success!</strong> {{ Session::get('successProfile') }}
									</div>
                            @endif
                            @if($errors->has())
                                    <div class="alert alert-error">
    										<a href="#" class="close" data-dismiss="alert">&times;</a>
    										<strong>Please correct the following errors!</strong> 
                                    @foreach($errors->all() as $message)
                                             <br /> {{ $message }} 
                                    @endforeach
                                    </div>
                            @endif
			         
                     
                     <div class="row-fluid">		
        		        <div class="span2">
                            @if(empty($user[0]->profile_pic))                           
                            <img alt="" style="margin:5px 0px 15px;" class="img-polaroid" src="{{URL::to('uploadsdata/default/defaultpic.jpg')}}" />
                            @else
                            <img alt="" style="margin:5px 0px 15px;" class="img-polaroid" src="{{URL::to('uploadsdata/'.Session::get('id').'/'.$user[0]->profile_pic)}}" />
                            @endif
                            <br />
                            <a href="./editprofile" class="btn btn-info pull-left">Edit profile</a>
                            
                        </div>          
                        <div class="span10">
                                    <fieldset>
                                    <legend>Personal Details</legend>
                                         <table border="0" align="left" width="100%" style="font-size: 13px;">
                                            <tr>
                                                <th width="30%">Full Name</th>
                                                <td>{{$user[0]->first_name.' '.$user[0]->middle_name.' '.$user[0]->last_name}}</td>
                                            </tr>
                                            <tr>
                                                <th>Email</th>
                                                <td>{{$user[0]->email}}</td>
                                            </tr>
                                            <tr>
                                                <th>Mobile</th>
                                                <td>{{$user[0]->mobile}}</td>
                                            </tr>
                                            <tr>
                                                <th>Gender</th>
                                                <td>{{$user[0]->gender}}</td>
                                            </tr>
                                            <tr>
                                                <th>Spouse</th>
                                                <td>{{$user[0]->spouse}}</td>
                                            </tr>
                                            <tr>
                                                <th>Place of Birth</th>
                                                <td>{{$user[0]->pob}}</td>
                                            </tr>
                                            <tr>
                                                <th>Country</th>
                                                <td>{{$user[0]->countryName}}</td>
                                            </tr>
                                            <tr>
                                                <th>State</th>
                                                <td>{{$user[0]->state}}</td>
                                            </tr>
                                            <tr>
                                                <th>City</th>
                                                <td>{{$user[0]->city}}</td>
                                            </tr>
                                            <tr>
                                                <th>Gotra</th>
                                                <td>{{$user[0]->gotra}}</td>
                                            </tr>
                                         </table>
                                         <legend>Family Details</legend>
                                         <table border="0" align="left" width="100%" style="font-size: 13px;">
                                            <tr>
                                                <th width="30%">Father First Name</th>
                                                <td>{{$user[0]->father_fname}}</td>
                                            </tr>
                                            <tr>
                                                <th>Mother First Name</th>
                                                <td>{{$user[0]->mother_fname}}</td>
                                            </tr>
                                            <tr>
                                                <th>Age</th>
                                                <td>{{$user[0]->age_range}}</td>
                                            </tr>
                                        </table>
                                    </fieldset>
                            
                        </div>		 
                    </div>
                     										 
                </div>
				
            </div>			


                </div>

            </div>

        <div id="footerInnerSeparator"></div>
    </div>

</div>
@stop
