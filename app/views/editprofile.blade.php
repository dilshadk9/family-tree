@extends('layouts.master')
@section('content')
<div id="contentOuterSeparator"></div>

<div class="container">

    <div class="divPanel page-content">

        <div class="row-fluid">

                <div class="span12" id="divMain">

                    <h1>Update Profile</h1> <hr />

                       <div class="row-fluid">
                <div class="span8" id="divMain">
                            @if (Session::get('successProfile'))	
									<div class="alert alert-success">
										<a href="#" class="close" data-dismiss="alert">&times;</a>
										<strong>Success!</strong> {{ Session::get('successProfile') }}
									</div>
                            @endif
                            @if($errors->has())
                                    <div class="alert alert-error">
    										<a href="#" class="close" data-dismiss="alert">&times;</a>
    										<strong>Please correct the following errors!</strong> 
                                    @foreach($errors->all() as $message)
                                             <br /> {{ $message }} 
                                    @endforeach
                                    </div>
                            @endif
			<!--Start Contact form -->		   
                                         
{{ Form::open(array('name' => 'profile', 'method' => 'post', 'id' => 'profile', 'files' => true )) }}
  <fieldset>
  <legend>Personal Details</legend>
	<span>First Name</span><input type="text" name="firstname" id="firstname" value="{{null !== Input::old('firstname')?Input::old('firstname'):$user[0]->first_name}}"  class="input-block-level" placeholder="First Name" />
	<span>Middle Name</span> <input type="text" name="middlename" id="middlename" value="{{null !== Input::old('middlename')?Input::old('middlename'):$user[0]->middle_name}}"  class="input-block-level" placeholder="Middle Name" />
	<span>Last Name</span><input type="text" name="lastname" id="lastname" value="{{null !== Input::old('lastname')?Input::old('lastname'):$user[0]->last_name}}"  class="input-block-level" placeholder="Last Name" />
    <span>Email</span><input type="text" name="email" id="email" value="{{null !== Input::old('email')?Input::old('email'):$user[0]->email}}" class="input-block-level" placeholder="Email" readonly="" />
    <span>Date of Birth</span><input type="text" name="dob" id="dob" value="{{null !== Input::old('dob')?Input::old('dob'):$user[0]->dob}}" class="input-block-level" placeholder="Date of Birth" />
    <span>Mobile</span><input type="text" name="mobile" id="mobile" value="{{null !== Input::old('mobile')?Input::old('mobile'):$user[0]->mobile}}" class="input-block-level" placeholder="Mobile" />
    <span>Gender</span>  <select class="form-control input-block-level" id="gender" name="gender">
        <option value="Male" {{{$user[0]->gender == 'Male' ? 'selected': ''}}}>Male</option>
        <option value="Female" {{{$user[0]->gender == 'Female' ? 'selected': ''}}}>Female</option>
        <option value="Other" {{{$user[0]->gender == 'Other' ? 'selected': ''}}}>Other</option>
      </select>
     <span>Spouse</span><input type="text" name="spouse" id="spouse" value="{{null !== Input::old('spouse')?Input::old('spouse'):$user[0]->spouse}}" class="input-block-level" placeholder="Spouse Name" /> 
    <span>Place of Birth</span><input type="text" name="pob" id="pob" value="{{null !== Input::old('pob')?Input::old('pob'):$user[0]->pob}}" class="input-block-level" placeholder="Place of birth" />
    <span>Country</span><select class="form-control input-block-level" name="country" id="country" onchange="getStateAndCity(this.value,1, 'state')">
		<option value="">Select Country</option>
		@foreach ($countries as $country)
			<option value="{{$country->location_id}}" {{{$user[0]->country == $country->location_id ? 'selected': ''}}}>{{$country->name}}</option>
		@endforeach
	</select>
	<span>State</span><select class="form-control input-block-level" name="state" id="state" onchange="getStateAndCity(this.value,2, 'city')">
	<option value="{{$user[0]->state_id}}">{{$user[0]->state}}</option>
	</select>
	<span>City</span><select class="form-control input-block-level" name="city" id="city">
	<option value="{{$user[0]->city_id}}">{{$user[0]->city}}</option>
	</select>
    <span>Gotra</span><input type="text" name="gotra" id="gotra" value="{{null !== Input::old('gotra')?Input::old('gotra'):$user[0]->gotra}}" class="input-block-level" placeholder="Gotra" />	
    <span>Profile pic</span><input type="file" name="profile_pic" id="profile_pic" value="{{null !== Input::old('profile_pic')?Input::old('profile_pic'):$user[0]->profile_pic}}" class="input-block-level" placeholder="Profile Pic" />
    @if(empty($user[0]->profile_pic))                           
    <img alt="" style="margin:5px 0px 15px;" class="img-polaroid" src="{{URL::to('uploadsdata/default/defaultpic.jpg')}}" />
    @else
    <img alt="" style="margin:5px 0px 15px;" class="img-polaroid" src="{{URL::to('uploadsdata/'.Session::get('id').'/'.$user[0]->profile_pic)}}" />
    @endif
    </fieldset>
	<fieldset>
    <legend>Family Details</legend>   
    <span>Father First Name</span><input type="text" name="father_fname" id="father_fname" value="{{null !== Input::old('father_fname')?Input::old('father_fname'):$user[0]->father_fname}}" class="input-block-level" placeholder="Father First Name" />
    <span>Mother First Name</span><input type="text" name="mother_fname" id="mother_fname" value="{{null !== Input::old('mother_fname')?Input::old('mother_fname'):$user[0]->mother_fname}}" class="input-block-level" placeholder="Mother First Name" />
    <span>Age</span><select class="form-control input-block-level" id="age_range" name="age_range">
        <option value="">Age</option>
        <option value="30-40" {{{$user[0]->age_range == '30-40' ? 'selected': ''}}}>30-40</option>
        <option value="40-50" {{{$user[0]->age_range == '40-50' ? 'selected': ''}}}>40-50</option>
        <option value="50-60" {{{$user[0]->age_range == '50-60' ? 'selected': ''}}}>50-60</option>
        <option value="60-70" {{{$user[0]->age_range == '60-70' ? 'selected': ''}}}>60-70</option>
        <option value="70-80" {{{$user[0]->age_range == '70-80' ? 'selected': ''}}}>70-80</option>
      </select>
    </fieldset>  
      
    <div class="actions">
	<input type="submit" value="Update profile" name="submit" id="submitButton" class="btn btn-info pull-left" title="Click here to update your profile!" />
	</div>
	
	
{{ Form::close() }}				 
			<!--End Contact form -->											 
                </div>
				
            </div>			


                </div>

            </div>

        <div id="footerInnerSeparator"></div>
    </div>

</div>
@stop
