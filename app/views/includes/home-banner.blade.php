<div id="decorative1" style="position:relative">
    <div class="container">

        <div class="divPanel headerArea">
            <div class="row-fluid">
                <div class="span12">

                        <div id="headerSeparator"></div>

                        <div id="divHeaderText" class="page-content">
                            <div id="divHeaderLine1">Your Header Text Here!</div><br />
                            <div id="divHeaderLine2">2nd line header text for calling extra attention to featured content..</div><br />
                            <div id="divHeaderLine3"><a class="btn btn-large btn-primary" href="#">More Info</a></div>
                        </div>

                        <div id="headerSeparator2"></div>

                </div>
            </div>

        </div>

    </div>
</div>