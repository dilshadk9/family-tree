<div id="decorative2">
    <div class="container">

        <div class="divPanel topArea notop nobottom">
            <div class="row-fluid">
                <div class="span12">

                    <div id="divLogo" class="pull-left">
                        <a href="./" id="divSiteTitle">FAMILY TREE</a><br />
                        <a href="./" id="divTagLine">Geneology & Family history records</a>
                    </div>

                    <div id="divMenuRight" class="pull-right">
                    <div class="navbar">
                        <button type="button" class="btn btn-navbar-highlight btn-large btn-primary" data-toggle="collapse" data-target=".nav-collapse">
                            NAVIGATION <span class="icon-chevron-down icon-white"></span>
                        </button>
                        <div class="nav-collapse collapse">
                            <ul class="nav nav-pills ddmenu">
                                <li class="dropdown {{{Request::path() == '/'? 'active': ''}}}"><a href="./">Home</a></li>
                                <li class="dropdown {{{Request::path() == 'aboutus'? 'active': ''}}}"><a href="./aboutus">About us</a></li>
                            @if (!Session::has('id'))
                             <li><a href="./login">Login</a></li>
                             @else
                               <li class="dropdown {{{Request::path() == 'profile' || Request::path() == 'myfamily'? 'active': ''}}}">
                                    <a href="./profile" class="dropdown-toggle">My Account <b class="caret"></b></a>
                                    <ul class="dropdown-menu">
                                    <li class="dropdown"><a href="./profile">Profile</a></li>
                                    @if(Session::has('myfamily'))
                                    <li class="dropdown"><a href="./myfamily">My Family</a></li>
                                    @endif
                                    <li class="dropdown"><a href="./logout">Logout</a></li> 
                                    </ul>
                                </li>
                             @endif 
                            </ul>
                             
                            
                        </div>
                    </div>
                    </div>

                </div>
            </div>
        </div>

    </div>
</div>