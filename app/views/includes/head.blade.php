<head>
    <meta charset="utf-8">
    <title>Family Tree - Welcome</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">  
	<META NAME="ROBOTS" CONTENT="NOINDEX, NOFOLLOW"> <!-- Remove this Robots Meta Tag, to allow indexing of site -->
    
    {{ HTML::style('assets/scripts/bootstrap/css/bootstrap.min.css'); }}
    {{ HTML::style('assets/scripts/bootstrap/css/bootstrap-responsive.min.css'); }}
    
    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      {{ HTML::script('http://html5shim.googlecode.com/svn/trunk/html5.js'); }}
    <![endif]-->

    <!-- Icons -->
    {{ HTML::style('assets/scripts/icons/general/stylesheets/general_foundicons.css'); }}
    {{ HTML::style('assets/scripts/icons/social/stylesheets/social_foundicons.css'); }}
    <!--[if lt IE 8]>
        {{ HTML::style('assets/scripts/icons/general/stylesheets/general_foundicons_ie7.css'); }}
        {{ HTML::style('assets/scripts/icons/social/stylesheets/social_foundicons_ie7.css'); }}
    <![endif]-->
    {{ HTML::style('assets/scripts/fontawesome/css/font-awesome.min.css'); }}
    <!--[if IE 7]>
        {{ HTML::style('assets/scripts/fontawesome/css/font-awesome-ie7.min.css'); }}
    <![endif]-->

    {{ HTML::style('assets/scripts/carousel/style.css'); }}
    {{ HTML::style('http://fonts.googleapis.com/css?family=Source+Sans+Pro'); }}
    {{ HTML::style('http://fonts.googleapis.com/css?family=Open+Sans'); }}
    {{ HTML::style('http://fonts.googleapis.com/css?family=Inconsolata'); }}
    {{ HTML::style('http://fonts.googleapis.com/css?family=Abel'); }}
    {{ HTML::style('http://fonts.googleapis.com/css?family=Open+Sans'); }}
    
    {{ HTML::style('assets/styles/custom.css'); }}
    {{ HTML::style('assets/styles/jquery-ui.css'); }}
    
</head>