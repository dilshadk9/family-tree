<html>
  <head>
	<style>
	.custom-css {
			color: green;
			width: 100px;
			background: #eee;
			text-align: center; 
	}
	google-visualization-orgchart-lineleft {
		border-left: 2px solid #333!important;
	}
	.google-visualization-orgchart-linebottom {
		border-bottom: 2px solid #333!important;
	}
	.google-visualization-orgchart-lineright {
		border-right: 2px solid #333!important;
	}
</style>
    <script type="text/javascript" src="https://www.google.com/jsapi"></script>
    <script type="text/javascript">
      google.load("visualization", "1", {packages:["orgchart"]});
      google.setOnLoadCallback(drawChart);
      function drawChart() {
        var data = new google.visualization.DataTable();
		
        data.addColumn('string', 'Name');
        data.addColumn('string', 'Manager');
        data.addColumn('string', 'ToolTip');

        data.addRows([
          [{v:'Mike', f:'Mike<div style="color:red; font-style:italic">Age: 46 <br> Addr: 3, SXD , Texas, US</div>'}, '', 'This is tooltip'],
          [{v:'Jim', f:'Jim<div style="color:red; font-style:italic">Vice President<div>'}, 'Mike', 'VP'],
          ['Alice', 'Mike', ''],
		  ['Alice2', 'Mike', ''],
		  ['Alice3', 'Mike', ''],
          ['Bob', 'Jim', 'Bob Spo	nge'],
		  ['Bob2', 'Jim', 'Bob Spo	nge'],
          ['Carol', 'Bob', '']
        ]);

        var chart = new google.visualization.OrgChart(document.getElementById('chart_div'));
        chart.draw(data, {allowHtml:true,size:'small',allowCollapse:true,nodeClass:'custom-css'});
		google.visualization.events.addListener(chart, 'select', function () {
			var selection = chart.getSelection();
			var message = "";
			var id = "";
				for (var i = 0; i < selection.length; i++) {
				  var item = selection[i];
				  if (item.row != null && item.column != null) {
					message += '{row:' + item.row + ',column:' + item.column + '}';
				  } else if (item.row != null) {
					message += '{row:' + data.getValue(item.row, 0) + '}';
					id = data.getValue(item.row, 0);
				  } else if (item.column != null) {
					message += '{column:' + item.column + '}';
				  }
				}
				if (message == '') {
				  message = 'nothing';
				}
				//alert('You selected ' + message);
			document.getElementById(id).style.display = 'block';
		});
      }
   </script>
    </head>
  <body>
    <div id="chart_div""></div>
	<div id="Mike" style="display:none;">Hey! I am Mike</div>
	<div id="Bob" style="display:none;">Hey! I am Bob</div>
	<div id="Carol" style="display:none;">Hey! I am Carol</div>
	<div id="Alice" style="display:none;">Hey! I am Alice</div>
	<div id="Jim" style="display:none;">Hey! I am Jim</div>
  </body>
</html>
