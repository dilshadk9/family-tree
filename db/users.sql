/*
SQLyog Enterprise - MySQL GUI v7.02 
MySQL - 5.6.20 : Database - family_tree
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

CREATE DATABASE /*!32312 IF NOT EXISTS*/`family_tree` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `family_tree`;

/*Table structure for table `users` */

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(120) NOT NULL,
  `middle_name` varchar(120) NOT NULL,
  `last_name` varchar(120) NOT NULL,
  `email` varchar(120) NOT NULL,
  `password` varchar(200) NOT NULL,
  `mobile` varchar(20) NOT NULL,
  `gender` varchar(10) NOT NULL,
  `spouse` varchar(100) DEFAULT NULL,
  `pob` varchar(160) DEFAULT NULL,
  `city` varchar(160) DEFAULT NULL,
  `state` varchar(160) DEFAULT NULL,
  `country` varchar(160) DEFAULT NULL,
  `father_fname` varchar(120) DEFAULT NULL,
  `mother_fname` varchar(120) DEFAULT NULL,
  `age_range` varchar(50) DEFAULT NULL,
  `gotra` varchar(120) DEFAULT NULL,
  `token` varchar(200) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `status` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`)
) ENGINE=MyISAM AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;

/*Data for the table `users` */

insert  into `users`(`id`,`first_name`,`middle_name`,`last_name`,`email`,`password`,`mobile`,`gender`,`spouse`,`pob`,`city`,`state`,`country`,`father_fname`,`mother_fname`,`age_range`,`gotra`,`token`,`created_at`,`updated_at`,`status`) values (1,'Dilshad','S','Khan','dilshadkhan68@gmail.com','$2y$10$b/5IL2MAyolyVcqrDrbW8.3Hx99.4fn7Xp1CB53gmq0Ofn2sqg4Dq','1234567890','Male','z','up','mumbai','maharashtra','india','s','l','40-50','xyz','KEIESfPsISwca5EfatjKQ4qoGx8kVLdCBMG4mfrn','2015-02-26 15:56:29','2015-02-26 10:26:29',1),(3,'asfd','asdf','asdfa','dilshadkhan6dd8@gmail.com','$2y$10$EPjI4UUgF2we6lWU7v/VxusMb.tc4jKBzrDDfFEFQay1kMKA0xfQy','1234567890','Male',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'z2zo10cOhmlTALYX3ofEqksl6atqaqGwbEYXEzCM','2015-02-15 17:21:46','2015-02-14 09:31:04',1),(4,'safd','sfd','sdf','sdsf@gmail.com','$2y$10$ZTqB8R.yAqOYos37z3AAMuO5DAx8hKJpYAKEY8yrXqYDERoQSChLm','1234567890','Male',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'z2zo10cOhmlTALYX3ofEqksl6atqaqGwbEYXEzCM','2015-02-15 17:21:46','2015-02-14 09:45:30',1),(5,'asfasdf','safdadsf','sdf','sdfsdf@gmail.com','$2y$10$Z6WlUUXGzl2dPmWwXcXCKu1Rto30Zpck8cvCjJI1sOd8NXj6CAVi.','1234567890','Male',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'z2zo10cOhmlTALYX3ofEqksl6atqaqGwbEYXEzCM','2015-02-15 17:21:46','2015-02-14 09:59:13',1),(6,'sadf','asdf','asdf','dilshadkhan222268@gmail.com','$2y$10$AMTt7EHqIM0.PX4mYzyVWe7jx5Q4u59ifFpOe2.UYmlbKpJFS4l9y','1234567890','Female',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'WYaDrvjHPjkXAgwxdXIkDY5DLwpwYwz5sRQ41KqU','2015-02-15 06:21:06','2015-02-15 06:21:06',0),(7,'sadf','asdf','asdf','dilshadkhan222222268@gmail.com','$2y$10$4V1ltoYouRvoqCO53tC9HuhHVS5S3Q8OO3uuOGOUHDQc/o01/ZG.e','1234567890','Female',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'WYaDrvjHPjkXAgwxdXIkDY5DLwpwYwz5sRQ41KqU','2015-02-15 06:21:45','2015-02-15 06:21:45',0),(8,'sadf','sdf','sdfsd','sddddfsdf@gmail.com','$2y$10$umklXjFgih/yrJyXVFUpBeAPnItsYLpBBZv.AtUJwNE.E4VBvF896','1234567890','Female',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'WYaDrvjHPjkXAgwxdXIkDY5DLwpwYwz5sRQ41KqU','2015-02-15 06:23:31','2015-02-15 06:23:31',0),(9,'asasdf','asdfsadf','asdfasd@gmail.com','sadfasdf@gmail.com','$2y$10$PzDZ15jnM/kyfEoGuoyXU.nSFuL/fzTH.zJ6pL5RYmCMBP87bkdWu','1234567890','Male',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'IHuJ5NHj4C3xPuBKQn1nvPumktAF9q3XNgiqNLkc','2015-02-16 15:34:13','2015-02-16 15:34:13',0),(10,'sadf','asdf','asdfasd','asdfsaf@gmail.com','$2y$10$SgkBAcxvOQrqb1ZRRz2BpObMCiYOgTi64WvQlPunwdP3LgIndXkfC','1234567890','Male',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'xCed7aGpmindiVsNRtCEoPERjDLAUem4JtrSQXSW','2015-02-21 06:36:50','2015-02-21 06:36:50',0),(11,'sfsadf','asdfsf','sadfsa','sdfsdf323@gmail.com','$2y$10$BT4PcHhuv.E3YFn8jtjhSOisAsg1vzXNi71RcCZNhKlxawZrvP8gm','1234567890','Male',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'xCed7aGpmindiVsNRtCEoPERjDLAUem4JtrSQXSW','2015-02-21 06:37:43','2015-02-21 06:37:43',0);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
