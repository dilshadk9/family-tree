/*
SQLyog Enterprise - MySQL GUI v7.02 
MySQL - 5.6.20 : Database - family_tree
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

CREATE DATABASE /*!32312 IF NOT EXISTS*/`family_tree` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `family_tree`;

/*Table structure for table `admin_auth` */

DROP TABLE IF EXISTS `admin_auth`;

CREATE TABLE `admin_auth` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(120) DEFAULT NULL,
  `password` varchar(120) DEFAULT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

/*Data for the table `admin_auth` */

insert  into `admin_auth`(`id`,`username`,`password`,`created_at`,`updated_at`) values (1,'admin','$2y$10$5KyAwQLoYJNUGjJbUnKDUOb1hcvmXEEc/31KTzlxrteGa54C4b372','2014-12-20 23:43:24','2014-12-20 23:43:24');

/*Table structure for table `users` */

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(120) NOT NULL,
  `middle_name` varchar(120) NOT NULL,
  `last_name` varchar(120) NOT NULL,
  `email` varchar(120) NOT NULL,
  `password` varchar(200) NOT NULL,
  `mobile` varchar(20) NOT NULL,
  `gender` varchar(10) NOT NULL,
  `token` varchar(200) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

/*Data for the table `users` */

insert  into `users`(`id`,`first_name`,`middle_name`,`last_name`,`email`,`password`,`mobile`,`gender`,`token`,`created_at`,`updated_at`) values (1,'Dilshad','S','Khan','dilshadkhan68@gmail.com','$2y$10$l9Cr7VrNCZy8K2r1l/AqWOYXLy988XXoz5oioFkxs43AOLHc0EIlC','1234567890','Male','KEIESfPsISwca5EfatjKQ4qoGx8kVLdCBMG4mfrn','2015-02-11 07:19:56','2015-02-11 07:19:56'),(3,'asfd','asdf','asdfa','dilshadkhan6dd8@gmail.com','$2y$10$EPjI4UUgF2we6lWU7v/VxusMb.tc4jKBzrDDfFEFQay1kMKA0xfQy','1234567890','Male','z2zo10cOhmlTALYX3ofEqksl6atqaqGwbEYXEzCM','2015-02-14 09:31:04','2015-02-14 09:31:04'),(4,'safd','sfd','sdf','sdsf@gmail.com','$2y$10$ZTqB8R.yAqOYos37z3AAMuO5DAx8hKJpYAKEY8yrXqYDERoQSChLm','1234567890','Male','z2zo10cOhmlTALYX3ofEqksl6atqaqGwbEYXEzCM','2015-02-14 09:45:30','2015-02-14 09:45:30'),(5,'asfasdf','safdadsf','sdf','sdfsdf@gmail.com','$2y$10$Z6WlUUXGzl2dPmWwXcXCKu1Rto30Zpck8cvCjJI1sOd8NXj6CAVi.','1234567890','Male','z2zo10cOhmlTALYX3ofEqksl6atqaqGwbEYXEzCM','2015-02-14 09:59:13','2015-02-14 09:59:13');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
