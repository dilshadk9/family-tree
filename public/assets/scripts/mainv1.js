$(document).ready(function(){
        $('#signup').submit(function(e){
            e.preventDefault();
            
            var formData = new FormData();
            formData.append('firstname', $('#firstname').val());
            formData.append('lastname', $('#lastname').val());
            formData.append('middlename', $('#middlename').val());
            formData.append('email', $('#email').val());
            formData.append('password', $('#password').val());
            formData.append('mobile', $('#mobile').val());
            formData.append('gender', $('#gender').val());
            formData.append('_token', $('input[name="_token"]').val());
            
            $.ajax({
               url: 'signup',
               method: 'post',
               processData: false,
               contentType: false,
               dataType: 'json',
               data: formData,
               headers: {
                'X-CSRF-Token': $('input[name="_token"]').val()
               },
               beforeSend: function() {
                $('#submitButton').val('Processing...');
                $('#submitButton').attr('disabled', 'disabled');
               },
               complete: function() {
                $('#submitButton').val('Signup');
                $('#submitButton').removeAttr('disabled');
               },	
               success: function(data){
                $('.alert').hide();
                if(data.success) {
                    $("#myModal").modal('show');
                    $('.continue').click(function(){
                        window.location = './signup';
                    });
                    
                }
                $.each(data.errors, function(index, error){
                   $('#'+index).after('<div class="alert alert-danger" role="alert">'+error+'</div>'); 
                });
               },
               error: function() {}
            });            
        });
        
        
   
});

function getStateAndCity(location_id,location_type, loadDataToDiv) {
	jQuery("#"+loadDataToDiv).html('<option value="">---Loading---</option>');
	// If you are changing counrty, make the state and city fields blank
	if(loadDataToDiv=='state'){
		jQuery('#city').html('<option value="">Select City</option>');
		jQuery('#state').html('<option value="">Select State</option>');
	}
	// If you are changing state, make the city fields blank
	if(loadDataToDiv=='city'){
		jQuery('#city').html('<option value="">Select City</option>');
	}
	var formData = new FormData();
    formData.append('location_id', location_id);
	formData.append('location_type', location_type);
	$.ajax({
			   url: 'getstateandcity',
			   method: 'post',
			   processData: false,
			   contentType: false,
			   data: formData,
			   headers: {
				'X-CSRF-Token': $('input[name="_token"]').val()
			   },
			   beforeSend: function() {
			   
			   },
			   complete: function() {
				
			   },	
			   success: function(result){
					jQuery('#' + loadDataToDiv).html(result);
			   },
			   error: function() {}
        });            
}

